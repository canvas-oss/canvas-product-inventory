# Copyright 2018-01-02
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'canvas_product_inventory'
require 'canvas_logger'
require 'mysql2'

module CanvasProductInventory
  # A class that define a CanvasProductInventory base Task
  class Task
    PROGRAM_NAME = 'canvas_product_inventory'.freeze
    # Base Task initializer
    def initialize
      @verbose = false
      # Define object finalizer
      ObjectSpace.define_finalizer(self, proc { finalize })
      # Initialize logger system
      init_canvas_log(6)
      # Load CANVAS configuration
      init_canvas_cfg
      # Connect to the `canvas_data` database
      init_canvas_data
    end

    # Load Syslog based CANVAS logger
    def init_canvas_log(facility)
      @canvas_log = CanvasLogger.new(PROGRAM_NAME, facility)
    rescue StandardError => e
      # Leave process in a such case
      abort("Cannot initialize CANVAS logger: #{e}")
    end

    # Load CANVAS Ccnfiguration
    def init_canvas_cfg
      @canvas_cfg = CanvasProductInventory::Configuration.new
    rescue NoMethodError => e
      @canvas_log.log('alert', "Invalid configuration file: #{e}")
      cli_puts("Invalid configuration file: #{e}")
    rescue IOError => e
      @canvas_log.log('error', "Cannot open configuration file: #{e}")
      cli_puts("Cannot open configuration file: #{e}")
    rescue StandardError => e
      @canvas_log.log('error', "Configuration file error: #{e}")
      cli_puts("Configuration file error: #{e}")
    end

    # Initialize the CANVAS `canvas_data` database connection
    def init_canvas_data
      @canvas_data = Mysql2::Client.new(@canvas_cfg.global[:database][:canvas_data])
    rescue Mysql2::Error => e
      @canvas_log.log('alert', "Cannot connect to canvas_data database: #{e}")
      abort("Cannot connect to canvas_data database: #{e}")
    rescue StandardError => e
      @canvas_log.log('error', "Cannot connect to canvas_data database: #{e}")
      abort("Cannot connect to canvas_data database: #{e}")
    end

    # Cleanup base Task resources
    def finalize
      # Close canvas_logger
      @canvas_log.close if @canvas_log.is_a? CanvasLogger
      # Close canvas database connection
      @canvas_data.close if @canvas_data.is_a? Mysql2::Client
    end

    # Helper : perform a SQL transaction
    def sql_transaction(sql_client)
      sql_client.query('BEGIN;')
      yield
      sql_client.query('COMMIT;')
    rescue Mysql2::Error => e
      # Log transaction error
      @canvas_log.log('error', "SQL transaction error: #{e}")
      cli_puts("SQL transaction error: #{e}")
      sql_client.query('ROLLBACK;')
    end

    # Helper print message if verbose
    def cli_puts(message)
      puts(message) if @verbose
    end
  end
end
