# Copyright 2018-01-02
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'canvas_product_inventory'
require 'canvas_product_inventory/version'
require 'canvas_product_inventory/update'

module CanvasProductInventory
  # Command Line Interface module
  module CLI
    EXECUTABLE_NAME = 'canvas-product-inventory'.freeze

    # Not found command
    def self.default
      # Print Error message
      "Command not found, see #{CanvasProductInventory::CLI::EXECUTABLE_NAME} --help"
    end

    # Status command. Print informations about the CANVAS system and product-inventory subsystem
    def self.status
      # Print CANVAS Subsystem Informations
      "Canvas Product Inventory version #{CanvasProductInventory::VERSION}"
    end

    # Update command. Update CANVAS CPE database from given file uri or from already configured file uri
    def self.update(verbose, uri)
      task = CanvasProductInventory::Update.new
      task.start(verbose, uri)
    end

    # Setup command. Setup CANVAS product-inventory subprocess. Create minimal required resources and add this subprocess into the CANVAS environment
    def self.setup(verbose)
      task = CanvasProductInventory::Setup
      task.start(verbose)
    end
  end
end
