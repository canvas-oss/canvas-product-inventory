# Copyright 2018-01-02
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'canvas_product_inventory'
require 'ox'

module CanvasProductInventory
  # A class that open a CPE dictionary XML database and parse it
  # @example Load a Cpe Dictionary from XML database file
  # => dictionary = CpeDictionary.new('./official-cpe-dictionary_v2.3.xml')
  # => dictionary.generator { |generator| ... }
  # => dictionary.cpe_item { |cpe_item| ... }
  class CpeDictionary
    # XML database charset is UTF-8
    DICTIONARY_CHARSET = 'UTF-8'.freeze
    # CPE Dictionary is a XML document
    def initialize(dictionary_uri)
      begin
        # Parse CPE Dictionary with Ox parser
        @dictionary = Ox.parse(File.open(dictionary_uri, "r:#{DICTIONARY_CHARSET}", &:read))
      rescue StandardError => e
        raise e
      end
      # Validate minimal database requirements
      check_dictionary
    end

    # CPE Dictionary is defined by one generator
    # Generator's metadata can be used in a block of code
    def generator
      # Parse the CPE generator
      generator = parse_generator(@dictionary.locate('cpe-list/generator')[0])
      # Process CPE generator
      yield generator
    end

    # CPE Dictionary have many CPE items
    # Each of these items can be processed in a block of code
    def each_item
      # Foreach CPE item
      @dictionary.locate('cpe-list/cpe-item').each do |element|
        # Parse CPE item
        begin
          # Extract CPE item data from XML database into a hash
          cpe_item = parse_cpe_item(element)
        rescue StandardError
          # Do not process malformed CPE item
          next
        end
        # Process each CPE item
        yield cpe_item
      end
    end

    # Validate CPE Dictionary XML database
    def check_dictionary
      # CPE Dictionary is a cpe-list
      raise StandardError unless @dictionary.locate('cpe-list').count == 1
      # CPE Dictionary contains exactly one generator
      raise StandardError unless @dictionary.locate('cpe-list/generator').count == 1
      # CPE Dictionary generator contains some data
      raise StandardError unless @dictionary.locate('cpe-list/generator/product_name').count == 1
      raise StandardError unless @dictionary.locate('cpe-list/generator/product_version').count == 1
      raise StandardError unless @dictionary.locate('cpe-list/generator/schema_version').count == 1
      raise StandardError unless @dictionary.locate('cpe-list/generator/timestamp').count == 1
    end

    # return a hash dezcribing a CPE dictionary generator
    def parse_generator(element)
      return {
        product_name: element.locate('product_name')[0].text,
        product_version: element.locate('product_version')[0].text,
        schema_version: element.locate('schema_version')[0].text,
        timestamp: Date.rfc3339(element.locate('timestamp')[0].text)
      }
    end

    # Return a hash describing a CPE item
    def parse_cpe_item(element)
      # Cpe Item
      cpe_item = {
        name: '',
        cpe23name: '',
        deprecated: false,
        deprecation_date: Date.new(0),
        deprecated_by: [],
        title: [],
        reference: [],
        check: [],
        note: []
      }
      # CPE description
      cpe_item[:name] = element['name'] unless element['name'].nil?
      cpe_item[:cpe23name] = element.locate('cpe-23:cpe23-item')[0]['name'] unless element.locate('cpe-23:cpe23-item')[0]['name'].nil?
      cpe_item[:deprecated] = element['deprecated'] unless element['deprecated'].nil?
      cpe_item[:deprecation_date] = Date.rfc3339(element['deprecation_date']) unless element['deprecation_date'].nil?
      # CPE title
      element.locate('title').each do |title|
        cpe_item[:title] << { lang: title['xml:lang'], value: title.text }
      end
      # CPE reference
      element.locate('references/reference').each do |reference|
        cpe_item[:reference] << { href: reference['href'], value: reference.text }
      end
      # CPE deprecation
      element.locate('cpe-23:deprecation').each do |deprecation|
        deprecation.locate('cpe-23:deprecated-by').each do |deprecated_by|
          cpe_item[:deprecated_by] << { name: deprecated_by['name'], type: deprecated_by['type'], date: Date.rfc3339(deprecation['date']) }
        end
      end
      # CPE check
      element.locate('check').each do |check|
        cpe_item[:check] << { system: check['system'], href: check['href'], value: check.text }
      end
      # CPE note
      element.locate('notes').each do |notes|
        notes.locate('note').each do |note|
          cpe_item[:note] << { lang: notes['xml:lang'], value: note.text }
        end
      end
      return cpe_item
    end

    private :check_dictionary, :parse_generator, :parse_cpe_item
  end
end
