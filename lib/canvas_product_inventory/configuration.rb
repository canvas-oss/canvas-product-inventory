# Copyright 2018-01-02
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'canvas_product_inventory'
require 'yaml'
require 'active_support/core_ext/hash'

module CanvasProductInventory
  # A class that open a Configuration file and parse it
  # @example Load configuration from a CANVAS product-inventory subprocess configuration file. A default file URI is assumed.
  # => configuration = new Configuration
  class Configuration
    # :global contains CANVAS global configuration as a hash
    attr_reader :global
    # :sp_cpi contains CANVAS product-inventory subprocess configuration as a hash
    attr_reader :sp_cpi

    # Default CANVAS configuration path
    PATH = File.expand_path('~/.config/canvas/').freeze
    # Default CANVAS product-inventory subprocess filename
    FILENAME = 'product-inventory.yml'.freeze

    # Load CANVAS configuration file from `uri` file or from default file
    def initialize(uri = nil)
      filename = File.realpath(File.join(PATH, FILENAME))
      filename = File.realpath(uri) if uri.is_a?(String) && uri.length.positive?
      # Load YAML configuration file
      documents = []
      YAML.load_stream(File.read(filename)) do |document|
        documents << document.deep_symbolize_keys
      end
      # CANVAS Configuration file contain CANVAS global configuration
      @global = load_global(documents[0])
      # CANVAS Configuration file contains CANVAS product-inventory subprocess configuration
      @sp_cpi = load_sp_cpi(documents[1])
    end

    # Load global configuration part from YAML file
    def load_global(hash)
      check_global(hash)
      return hash[:canvas]
    end

    # Ensure that CANVAS configuration contains at least some parameters
    def check_global(hash)
      # CANVAS global configuration contains `canvas`
      hash.try!(:[], :canvas)
      # CANVAS global configuration contains database parameters
      hash[:canvas].try!(:[], :database)
      # CANVAS global configuration contains `canvas_data` database parameters
      hash[:canvas][:database].try!(:[], :canvas_data)
      hash[:canvas][:database][:canvas_data].try!(:[], :host)
      hash[:canvas][:database][:canvas_data].try!(:[], :port)
      hash[:canvas][:database][:canvas_data].try!(:[], :database)
      hash[:canvas][:database][:canvas_data].try!(:[], :username)
      hash[:canvas][:database][:canvas_data].try!(:[], :password)
      # CANVAS global configuration contains `canvas_configuration` database parameters
      hash[:canvas][:database].try!(:[], :canvas_configuration)
      hash[:canvas][:database][:canvas_configuration].try!(:[], :host)
      hash[:canvas][:database][:canvas_configuration].try!(:[], :port)
      hash[:canvas][:database][:canvas_configuration].try!(:[], :database)
      hash[:canvas][:database][:canvas_configuration].try!(:[], :username)
      hash[:canvas][:database][:canvas_configuration].try!(:[], :password)
    end

    # Load subprocess configuration part from YAML file
    def load_sp_cpi(hash)
      check_sp_cpi(hash)
      return hash[:canvas_product_inventory]
    end

    # Ensure that CANVAS subprocess configuration contains at least some parameters
    def check_sp_cpi(hash)
      # CANVAS product-inventory subprocess configuration contains `canvas_product_inventory`
      hash.try!(:[], :canvas_product_inventory)
      # CANVAS product-inventory subprocess configuration contains sources configuration
      hash[:canvas_product_inventory].try!(:[], :cpe_dictionary)
      hash[:canvas_product_inventory][:cpe_dictionary].try!(:[], :source)
    end
  end
end
