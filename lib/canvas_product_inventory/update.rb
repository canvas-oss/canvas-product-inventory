# Copyright 2018-01-02
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'canvas_product_inventory'
require 'canvas_product_inventory/task'
require 'canvas_product_inventory/configuration'
require 'canvas_product_inventory/cpe_dictionary'
require "net/http"
require "openssl"
require 'zlib'
require 'ox'
require 'tempfile'
require 'mysql2'

module CanvasProductInventory
  # A class that define the Update process of CanvasProductInventory
  class Update < CanvasProductInventory::Task
    DICTIONARY_TEMPFILE_NAME = 'cpe_dictionary'.freeze
    # Entry point of update task
    def start(verbose, uri)
      @verbose = true if verbose == true
      # Load file from configured URI if given URI is invalid
      uri = load_uri_from_cfg unless uri.is_a?(String) && uri.length > 0
      # Save file in a temporary local file
      dictionary_uri = load_file_from_uri(uri)
      # Process dictionary
      process_dictionary(dictionary_uri)
    end

    # Load URI from configuration file
    def load_uri_from_cfg
      return File.join(@canvas_cfg.sp_cpi[:cpe_dictionary][:source][:repository], @canvas_cfg.sp_cpi[:cpe_dictionary][:source][:file_gz]) unless @canvas_cfg.sp_cpi[:cpe_dictionary][:source][:repository].nil? || @canvas_cfg.sp_cpi[:cpe_dictionary][:source][:file_gz].nil?
    end

    # Load XML file from a given URI
    def load_file_from_uri(uri)
      begin
        local_file = Tempfile.new(DICTIONARY_TEMPFILE_NAME)
        local_file_uri = local_file.path
      rescue StandardError => e
        @canvas_log.log('error', "Error while creating temporary file: #{e}")
        cli_puts("Error while creating temporary file: #{e}")
      end
      attempts = 1
      attempts = @canvas_cfg.sp_cpi[:http][:attempts] if @canvas_cfg.sp_cpi.try(:http).try(:attemps) && @canvas_cfg.sp_cpi[:http][:attempts].positive?
      begin
        attempts -= 1
        cli_puts("Downloading file: #{uri} ...")
        puri = URI.parse(uri)
        Net::HTTP.start(puri.host, puri.port, { use_ssl: true }) do |http|
          response = http.get(puri.request_uri)
          local_file.write(Zlib::GzipReader.new(StringIO.new(response.body.to_s)).read)
        end
      rescue StandardError => e
        retry if attempts > 0
        @canvas_log.log('error', "Error while loading remote CPE dictionary file: #{e}")
        cli_puts("Error while loading remote CPE dictionary file: #{e}")
      ensure
        local_file.close
      end
      return local_file_uri
    end

    # Process whole CPE dictionary
    def process_dictionary(uri)
      begin
        # Parse XML dictionary
        @cpe_dictionary = CanvasProductInventory::CpeDictionary.new(uri)
      rescue StandardError => e
        @canvas_log.log('error', "Error while loading local CPE dictionary file: #{e}")
        cli_puts("Error while loading local CPE dictionary file: #{e}")
      end
      begin
        # Save start of the update process
        generator_id = process_generator
        # Save each CPE item
        process_cpe_item(generator_id)
      rescue StandardError => e
        @canvas_log.log('error', "CPE dictionary parsing error: #{e}")
        cli_puts("CPE dictionary parsing error: #{e}")
      ensure
        # Save end of the update process
        process_complete(generator_id) unless generator_id.nil?
      end
    end

    # Declare start of a new update processa nd save CPE dictionary metadata
    def process_generator
      # Prepare SQL Query
      p_cpe_start_update_process = @canvas_data.prepare('CALL p_cpe_start_update_process (?, ?, ?, ?, @generator_id);')
      retrieve_generator_id = @canvas_data.prepare('SELECT @generator_id AS generator_id;')
      generator_id = nil
      sql_transaction(@canvas_data) do
        # Save generator in CANVAS database
        @cpe_dictionary.generator do |generator|
          # Execute SQL Query
          p_cpe_start_update_process.execute(generator[:timestamp].to_s, generator[:schema_version], generator[:product_name], generator[:product_version])
          # Retrieve generator id
          result = retrieve_generator_id.execute
          generator_id = result.first['generator_id']
        end
      end
      return generator_id
    end

    # Save each items from CPE dictionary
    def process_cpe_item(generator_id)
      # Prepare SQL statements
      p_cpe_save_cpe = @canvas_data.prepare('CALL p_cpe_save_cpe (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, @cpe_id);')
      p_cpe_save_cpe_title = @canvas_data.prepare('CALL p_cpe_save_cpe_title (?, ?, ?, ?);')
      p_cpe_save_cpe_reference = @canvas_data.prepare('CALL p_cpe_save_cpe_reference (?, ?, ?, ?);')
      p_cpe_save_cpe_deprecated_by = @canvas_data.prepare('CALL p_cpe_save_cpe_deprecated_by (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);')
      retrieve_cpe_id = @canvas_data.prepare('SELECT @cpe_id AS cpe_id;')
      # Foreach CPE item
      @cpe_dictionary.each_item do |cpe_item|
        sql_transaction(@canvas_data) do
          wfn = cpe23name_to_wfn(cpe_item[:cpe23name])
          cpe_item_deprecated = '0'
          cpe_item_deprecated = '1' if cpe_item[:deprecated]
          # Save CPE
          p_cpe_save_cpe.execute(wfn[:part], wfn[:vendor], wfn[:product], wfn[:version], wfn[:update], wfn[:edition], wfn[:sw_edition], wfn[:target_sw], wfn[:target_hw], cpe_item_deprecated, cpe_item[:deprecation_date].to_s, generator_id)
          result = retrieve_cpe_id.execute
          cpe_item_id = result.first['cpe_id']
          # Save CPE title
          cpe_item[:title].each do |title|
            p_cpe_save_cpe_title.execute(cpe_item_id, title[:lang], title[:value], generator_id)
          end
          # Save CPE reference
          cpe_item[:reference].each do |reference|
            p_cpe_save_cpe_reference.execute(cpe_item_id, reference[:href], reference[:value], generator_id)
          end
          # Save CPE deprecated_by
          cpe_item[:deprecated_by].each do |deprecated_by|
            wfn = cpe23name_to_wfn(deprecated_by[:name])
            p_cpe_save_cpe_deprecated_by.execute(cpe_item_id, wfn[:part], wfn[:vendor], wfn[:product], wfn[:version], wfn[:update], wfn[:edition], wfn[:sw_edition], wfn[:target_sw], wfn[:target_hw], deprecated_by[:date].to_s, deprecated_by[:type], generator_id)
          end
        end
      end
    end

    # Save end of update process
    def process_complete(generator_id)
      # Prepare SQL statement
      p_cpe_complete_update_process = @canvas_data.prepare('CALL p_cpe_complete_update_process (?);')
      # Execute SQL Query
      p_cpe_complete_update_process.execute(generator_id)
    end

    # Helper: return CPE2.3 name as a WFN hash
    def cpe23name_to_wfn(name)
      name_split = name.split(':')
      return {
        part: name_split[2],
        vendor: name_split[3],
        product: name_split[4],
        version: name_split[5],
        update: name_split[6],
        edition: name_split[7],
        sw_edition: name_split[8],
        target_sw: name_split[9],
        target_hw: name_split[10]
      }
    end
  end
end
