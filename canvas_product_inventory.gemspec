# Copyright 2018-01-02
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'canvas_product_inventory/version'

Gem::Specification.new do |spec|
  spec.name          = 'canvas_product_inventory'
  spec.version       = CanvasProductInventory::VERSION
  spec.authors       = ['Valentin PRODHOMME']
  spec.email         = ['valentin@prodhomme.me']
  spec.date          = '2018-01-02'
  spec.license       = 'CeCILL'

  spec.summary       = 'API for CANVAS Product Inventory subsystem'
  spec.description   = 'Parse CPE file from NVD and save it into SQL database'
  spec.homepage      = 'https://bitbucket.org/canvas-oss/canvas-product-inventory'

  spec.files         = Dir['{bin,lib,test}/**/*', 'LICENSE.CECILL-EN', 'LICENSE.CECILL-FR', 'README.md']

  spec.require_paths = ['lib']
  spec.executables   = ['canvas-product-inventory']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_runtime_dependency 'activesupport'
  spec.add_runtime_dependency 'canvas_logger'
  spec.add_runtime_dependency 'mysql2'
  spec.add_runtime_dependency 'ox'
end
