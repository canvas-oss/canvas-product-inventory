# CanvasProductInventory

## Configuration

You can copy and edit the example configuration file (`files/product-inventory.yml.example `) into `~/.config/canvas/product-inventory.yml`.

## Usage

`canvas-product-inventory -c update`